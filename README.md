# Student Management System

This is a simple Student Management System web application developed using Java, JSP, Servlets, and MySQL. The project uses the Maven build automation tool.

## Table of Contents

- [Features](#features)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Running the Application](#running-the-application)
- [Project Structure](#project-structure)
- [Technologies Used](#technologies-used)

## Features

- View a list of students
- Add a new student
- Edit student details
- Delete a student

## Prerequisites

- Java Development Kit (JDK) 11 or higher
- Apache Maven
- Apache Tomcat 10.1.24
- MySQL Server

## Installation

1. **Clone the Repository:**

   ```sh
   git clone https://github.com/your-username/student-management-system.git
   cd student-management-system

2. **Set up the Database**


    ```sql
        CREATE DATABASE studentdb;

        USE studentdb;

        CREATE TABLE students (
            id INT NOT NULL AUTO_INCREMENT,
            name VARCHAR(100) NOT NULL,
            email VARCHAR(100) NOT NULL,
            course VARCHAR(100) NOT NULL,
            PRIMARY KEY (id)
        );

3. **Update Database Credentials:**

Update the database credentials in `DBConnection.java`:   

```java
String dbURL = "jdbc:mysql://localhost:3306/"; // Use Your mysql port number
String dbName = "studentdb";
String dbUsername = "root";
String dbPassword = "yourpassword"; // Use your MySQL root password 
```

4. **Build the Project:**

Navigate to the project directory and build the project using Maven:

```sh
mvn clean install
```


## Running the Application

1. **Deploy the WAR File:**
Copy the generated WAR file from the target directory to the webapps directory of your Tomcat installation:

```sh
cp target/StudentManagementSystem.war /path/to/tomcat/webapps/
```

2.  **Start Tomcat:**
Start the Tomcat server:

```sh
/path/to/tomcat/bin/startup.sh
```
    

3. **Access the web application:**
Open a web browser and navigate to [http://localhost:8080/StudentManagementSystem.](http://localhost:8080/StudentManagementSystem.)


## Project Structure
    StudentManagementSystem/
        ├── src/
        │   ├── main/
        │   │   ├── java/
        │   │   │   └── com/
        │   │   │       └── example/
        │   │   │           └── student/
        │   │   │               ├── controller/
        │   │   │               │   └── StudentController.java
        │   │   │               ├── dao/
        │   │   │               │   └── StudentDAO.java
        │   │   │               ├── model/
        │   │   │               │   └── Student.java
        │   │   │               └── util/
        │   │   │                   └── DBConnection.java
        │   │   ├── resources/
        │   │   └── webapp/
        │   │       ├── WEB-INF/
        │   │       │   └── web.xml
        │   │       ├── index.jsp
        │   │       ├── addStudent.jsp
        │   │       ├── editStudent.jsp
        │   │       └── listStudents.jsp
        └── pom.xml

## Technologies Used
- Java
- Jakarta Servlet API
- Jakarta JSP API
- Apache Maven
- MySQL
- Apache Tomcat