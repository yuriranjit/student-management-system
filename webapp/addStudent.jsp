<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add Student</title>
    <script type="text/javascript">
        function validateForm() {
            var name = document.forms["studentForm"]["name"].value;
            var email = document.forms["studentForm"]["email"].value;
            var course = document.forms["studentForm"]["course"].value;
            
            if (name == "" || email == "" || course == "") {
                alert("All fields must be filled out");
                return false;
            }
            
            var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
            if (!emailPattern.test(email)) {
                alert("Invalid email format");
                return false;
            }
            
            return true;
        }
    </script>
</head>
<body>
    <h1>Add New Student</h1>
    <form name="studentForm" action="insert" method="post" onsubmit="return validateForm()">
        Name: <input type="text" name="name"><br>
        Email: <input type="text" name="email"><br>
        Course: <input type="text" name="course"><br>
        <input type="submit" value="Add">
    </form>
    <a href="index.jsp">Back</a>
</body>
</html>
