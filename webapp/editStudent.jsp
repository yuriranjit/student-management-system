<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Edit Student</title>
    <script type="text/javascript">
        function validateForm() {
            var name = document.forms["studentForm"]["name"].value;
            var email = document.forms["studentForm"]["email"].value;
            var course = document.forms["studentForm"]["course"].value;
            
            if (name == "" || email == "" || course == "") {
                alert("All fields must be filled out");
                return false;
            }
            
            var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
            if (!emailPattern.test(email)) {
                alert("Invalid email format");
                return false;
            }
            
            return true;
        }
    </script>
</head>
<body>
    <h1>Edit Student</h1>
    <form name="studentForm" action="update" method="post" onsubmit="return validateForm()">
        <input type="hidden" name="id" value="${student.id}">
        Name: <input type="text" name="name" value="${student.name}"><br>
        Email: <input type="text" name="email" value="${student.email}"><br>
        Course: <input type="text" name="course" value="${student.course}"><br>
        <input type="submit" value="Update">
    </form>
    <a href="index.jsp">Back</a>
</body>
</html>
