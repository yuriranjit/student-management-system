<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="com.example.student.model.Student" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>List of Students</title>
</head>
<body>
    <h1>List of Students</h1>
    <table border="1" cellpadding="10" cellspacing="1">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Course</th>
            <th>Actions</th>
        </tr>
        <c:forEach var="student" items="${listStudent}">
            <tr>
                <td>${student.id}</td>
                <td>${student.name}</td>
                <td>${student.email}</td>
                <td>${student.course}</td>
                <td>
                    <a href="edit?id=${student.id}">Edit</a>
                    <a href="delete?id=${student.id}">Delete</a>
                </td>
            </tr>
        </c:forEach>
    </table>
    <a href="index.jsp">Back</a>
</body>
</html>
