package com.example.student.dao;

import com.example.student.model.Student;
import com.example.student.util.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StudentDAO {
    public static int save(Student student) throws SQLException, ClassNotFoundException {
        Connection con = DBConnection.initializeDatabase();
        String query = "INSERT INTO students (name, email, course) VALUES (?, ?, ?)";
        PreparedStatement ps = con.prepareStatement(query);
        ps.setString(1, student.getName());
        ps.setString(2, student.getEmail());
        ps.setString(3, student.getCourse());
        int status = ps.executeUpdate();
        con.close();
        return status;
    }

    public static int update(Student student) throws SQLException, ClassNotFoundException {
        Connection con = DBConnection.initializeDatabase();
        String query = "UPDATE students SET name=?, email=?, course=? WHERE id=?";
        PreparedStatement ps = con.prepareStatement(query);
        ps.setString(1, student.getName());
        ps.setString(2, student.getEmail());
        ps.setString(3, student.getCourse());
        ps.setInt(4, student.getId());
        int status = ps.executeUpdate();
        con.close();
        return status;
    }

    public static int delete(int id) throws SQLException, ClassNotFoundException {
        Connection con = DBConnection.initializeDatabase();
        String query = "DELETE FROM students WHERE id=?";
        PreparedStatement ps = con.prepareStatement(query);
        ps.setInt(1, id);
        int status = ps.executeUpdate();
        con.close();
        return status;
    }

    public static Student getStudentById(int id) throws SQLException, ClassNotFoundException {
        Connection con = DBConnection.initializeDatabase();
        String query = "SELECT * FROM students WHERE id=?";
        PreparedStatement ps = con.prepareStatement(query);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        Student student = new Student();
        if (rs.next()) {
            student.setId(rs.getInt("id"));
            student.setName(rs.getString("name"));
            student.setEmail(rs.getString("email"));
            student.setCourse(rs.getString("course"));
        }
        con.close();
        return student;
    }

    public static List<Student> getAllStudents() throws SQLException, ClassNotFoundException {
        Connection con = DBConnection.initializeDatabase();
        String query = "SELECT * FROM students";
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        List<Student> list = new ArrayList<>();
        while (rs.next()) {
            Student student = new Student();
            student.setId(rs.getInt("id"));
            student.setName(rs.getString("name"));
            student.setEmail(rs.getString("email"));
            student.setCourse(rs.getString("course"));
            list.add(student);
        }
        con.close();
        return list;
    }
}
