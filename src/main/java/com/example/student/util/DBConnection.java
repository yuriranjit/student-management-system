package com.example.student.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    public static Connection initializeDatabase() throws SQLException, ClassNotFoundException {
        String dbDriver = "com.mysql.jdbc.Driver";
        String dbURL = "jdbc:mysql://localhost:3306/";
        String dbName = "studentdb";
        String dbUsername = "root";
        String dbPassword = "password";

        Class.forName(dbDriver);
        Connection connection = DriverManager.getConnection(dbURL + dbName, dbUsername, dbPassword);
        return connection;
    }
}
